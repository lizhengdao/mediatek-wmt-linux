#include <string.h>

#define PROPERTY_VALUE_MAX 4096

#define ALOGI(fmt, ...) fprintf(stdout, "[mtk-loader][inf] " fmt "\n", ##__VA_ARGS__)
#define ALOGD(fmt, ...) fprintf(stdout, "[mtk-loader][dbg] " fmt "\n", ##__VA_ARGS__)
#define ALOGE(fmt, ...) fprintf(stderr, "[mtk-loader][err] " fmt "\n", ##__VA_ARGS__)

static inline int property_get(const char *key, char *value, const char *default_value)
{
    int len = 0;
    if(default_value) {
        len = strlen(default_value);
        memcpy(value, default_value, len + 1);
    }
    return len;
}

static inline int property_set(const char *key, const char *value)
{
    return 0;
}